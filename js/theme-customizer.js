/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 * Things like site title and description changes.
 */

( function( $ ) {
	"use strict";

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' == to ) {
				if ( 'remove-header' == _wpCustomizeSettings.values.header_image )
					$( '.home-link' ).css( 'min-height', '0' );
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.home-link' ).css( 'min-height', '230px' );
				$( '.site-title, .site-description' ).css( {
					'clip': 'auto',
					'color': to,
					'position': 'relative'
				} );
			}
		} );
	} );

	// OM Mod stuff:
	wp.customize( 'show_site_logo', function( value ){
		value.bind( function (to) {
			$('#masthead a.home-link > img').remove();
			console.log(_wpCustomizeSettings);
			if (to === true) {
				if (_wpCustomizeSettings.values.site_logo) {
					$('#masthead a.home-link').prepend('<img src="' + _wpCustomizeSettings.values.site_logo+ '"/>');
				} else {
					$('#masthead a.home-link').prepend('<img src="" alt="Default Logo" />');
				}
			}
		});
	});

	wp.customize('site_logo', function(value) {
		value.bind( function (to) {
			_wpCustomizeSettings.values.site_logo = to;
			$('#masthead a.home-link > img')[0].src = to; 	
			console.log(to);
		});
	});

} )( jQuery );
