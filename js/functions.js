/**
 * Functionality specific to OM 2014.
 *
 * Provides helper functions to enhance the theme experience.
 */

(function($) {

"use strict";

var body    = $( 'body' ),
    _window = $( window );


// endsWith string polyfill.

if (!String.prototype.hasOwnProperty('endsWith')) {
    String.prototype.endsWith = function(searchString, position) {
        if (position === undefined || position > this.length) {
            position = this.length;
        }
        position -= searchString.length;
        return (this.indexOf(searchString, position) !== -1)

    }
}


/**
 * Adds a top margin to the footer if the sidebar widget area is higher
 * than the rest of the page, to help the footer always visually clear
 * the sidebar.
 */
$( function() {
    if ( body.is( '.sidebar' ) ) {
        var sidebar   = $( '#secondary .widget-area' ),
            secondary = ( 0 == sidebar.length ) ? -40 : sidebar.height(),
            margin    = $( '#tertiary .widget-area' ).height() - $( '#content' ).height() - secondary;

        if ( margin > 0 && _window.innerWidth() > 999 )
            $( '#colophon' ).css( 'margin-top', margin + 'px' );
    }
} );

/**
 * Enables menu toggle for small screens.
 */
$('.menu-toggle').click(function() {
    $(this).parents('.nav').toggleClass('unclicked');
});

/**
 * Makes "skip to content" link work correctly in IE9 and Chrome for better
 * accessibility.
 *
 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
 */
 _window.on( 'hashchange.om2014', function() {
    var element = document.getElementById( location.hash.substring( 1 ) );

    if ( element ) {
        if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
            element.tabIndex = -1;

        element.focus();
    }
} );

/**
 * Arranges footer widgets vertically.
 * If we're not using masonry plugin, this could probably be removed?
 */
if ( $.isFunction( $.fn.masonry ) ) {
    var columnWidth = body.is( '.sidebar' ) ? 228 : 245;

    $( '#secondary .widget-area' ).masonry( {
        itemSelector: '.widget',
        columnWidth: columnWidth,
        gutterWidth: 20,
        isRTL: body.is( '.rtl' )
    } );
}

// om scroller stuff:
/* Done in CSS now
function reduce_font_size_to_fit(inner, outer) {
    'use strict';
    // reduce fontsize until inner.height() < outer.height()...

    var percent = 100;
    var zone_height = $(outer).height();
    var i = 100;
    var height = inner.height();

    if ( height > zone_height ) {
        while(i>1){
            height = inner.height();
            i = i / 2;
            if (height < zone_height) {
                percent += i;
            } else if (height > zone_height) {
                percent -= i;
            }
            inner.css('font-size', percent + '%');
        }
        inner.css('font-size', parseInt(percent) + '%');
    }
}

function denomination(cssvalue) {
    if (cssvalue.endsWith('%')) return '%';
    if (cssvalue.endsWith('em')) return 'em';
    if (cssvalue.endsWith('px')) return 'px';
    if (cssvalue.endsWith('pt')) return 'pt';
}

function reduce_font_width_to_fit(inner, outer) {
    'use strict';
    // reduce fontsize until inner.height() < outer.height()...

    var percent = parseFloat(inner.css('font-size'));
    var zone_width = $(outer).width();
    var i = percent;
    var width = inner.width();
    var denom = denomination(inner.css('font-size'));

    if ( width > zone_width ) {
        while(i>0.3){
            width = inner.width();
            i = i / 2;
            if (width < zone_width) {
                percent += i;
            } else if (width > zone_width) {
                percent -= i;
            }
            inner.css('font-size', percent + denom);
        }
        inner.css('font-size', parseInt(percent) + denom);
    }
}
*/

    $('.omarea').hover( function () {
        $(this).children('.desc').css('opacity',1);
    }, function() {
        $(this).children('.desc').css('opacity',0);
    }).each(function() {
        var that = $(this),
            desc = that.children('.desc');
		/* Done in CSS
        $(window).load(function(){

            reduce_font_width_to_fit(desc.children('h3'), that);

            //reduce_font_size_to_fit(that.children('.desc').first(), that);
            reduce_font_size_to_fit(that.children('.title').first(), that);
            });
        */    
    });

    (function () {
        var scrollbox_stop = false;

        $('.scrollbox').hover(function(){scrollbox_stop=true;},
                              function(){scrollbox_stop=false;})
                       .css('opacity', 1)
                       .slideDown('fast')
                       .children().first().siblings().hide();

        setInterval(function() {
        if (scrollbox_stop) { return; }

        $('.scrollbox').map(function(){
            var that = $(this),
            height = that.height();

            // don't keep growing and shrinking. that's annoying.

            if (height > parseInt(that.css('min-height'), 10)) {
            that.css('min-height', height);
            }

            // swap which children are visible

            that.children(':visible').fadeOut(1000, function () {

            var next = $(this).next();
            if (next.length){
                next.fadeIn();
            } else {
                that.children().first().fadeIn(700);
            }
            });
        });
        }, 9000);
    })();


    // and now as a jQuery plugin.

    $.fn.scrollbox = function (show_time) {
        var scrollbox = this;

        scrollbox.scrollbox_stop = false;

         scrollbox.hover(function(){scrollbox.scrollbox_stop=true;},
                         function(){scrollbox.scrollbox_stop=false;}).css('opacity', 1)
                  .slideDown('fast');

             scrollbox.children().first().addClass('scrollbox_shown').siblings().css('display', 'none');

        setInterval(function() {

            var height = scrollbox.height();

            if (scrollbox.scrollbox_stop) { return; }

            // don't keep growing and shrinking. that's annoying.

            if (height > parseInt(scrollbox.css('min-height'), 10)) {
                scrollbox.css('min-height', height);
            }

        // swap which children are visible
        if (scrollbox.children().length > 1) {
        scrollbox.children('.scrollbox_shown').removeClass('scrollbox_shown').fadeOut(1000,
            function () {

                var next = $(this).next('div');
                if (next.length){
                    next.addClass('scrollbox_shown').fadeIn();
                } else {
                    scrollbox.children('div').first().addClass('scrollbox_shown').fadeIn();
                }
            });
    }
        }, show_time ? show_time : 7000);

    };


    // Make posts on the same row have the same height.

    function post_resizes() {
        var arts = $('article');
        arts.each(function(){
            var me = $(this),
                //sibs = me.siblings(),
                sibs = arts,
                my_top = me.offset()['top'],
                line_sibs = sibs.filter(function(i){
                        return $(this).offset()['top'] === my_top;
                    }).each(function(){
                        if ($(this).height() > me.height()){
                            me.height($(this).height());
                        }
                    });
        });
    };
    setTimeout(post_resizes,1000);
    $(window).resize(post_resizes);

} )( jQuery );
