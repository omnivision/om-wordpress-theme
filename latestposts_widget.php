<?php
/**
 * Plugin Name: Latest Posts (as article boxes) Widget
 * Description: Shows latest posts in boxes. Like articles.
 * Version: 0.1
 * Author: Daniel Fairhead
 * Author URI: http://www.om.org/
 */

add_action( 'widgets_init', 'register_latest_posts_widget' );

function register_latest_posts_widget() {
	register_widget( 'Latest_Posts_Widget' );
}

class LatestCollapsable {
    /* In a widget, if you want a section to be collapsable, use this.
        Collapsable::start('Links');
           echo 'stuff inside the widget...';
        Collapsable::end();
    */
    public function start($title, $id) {
        echo '<div class="widget ' . $id . '"><div class="widget-top" style="cursor:pointer"><div class="widget-title-action"><a class="widget-action hide-if-no-js" href="#"></a></div><div class="widget-title"><h5>', __($title, 'latest_posts_widget'), '</h5></div></div><div class="widget-inside">';
    }
    public function end() {
        echo '</div></div>';
    }

}


class Latest_Posts_Widget extends WP_Widget {

	function Latest_Posts_Widget() {
		$widget_ops = array( 'classname' => 'latest_posts_widget',
		                   'description' => __('A simple widget for showing the latest posts as items, not just a list.', 'latest_posts_widget'));
		$control_ops = array('height' => 300,
		                     'id_base' => 'latest_posts_widget',
                             'categories'=>array());

		$this->WP_Widget( 'latest_posts_widget', __('Latest Posts Widget', 'latest_posts_widget'), $widget_ops, $control_ops);

	}

	function widget( $args, $instance ) {
		extract ($args);
		$show_info = isset( $instance['show_info'] ) ? $instance['show_info'] : false;

		echo $before_widget;

		?>
			<div class="latest_posts_widget-box group" style="width:80%;margin:auto;">
        <?php
                add_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);
                $query = new WP_Query(array('category__in'=>$instance['categories'], 'posts_per_page' =>3));
                remove_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);

                while ($query->have_posts()) {
                $query->the_post();

                ?>
                <div class="col span_1_of_3"><article class="latest_posts_widget_post post">
                    <header class="entry-header">
                        <a href="<?php echo get_permalink(); ?>">
                          <h1 class="entry-title"><?php the_title(); ?></h1>
                          <?php the_post_thumbnail(); ?>
                          </a>
                    </header>
                    <div class="entry-summary">
                          <?php the_excerpt();?>
                    <a class="more-link" href="<?php echo get_permalink(); ?>"><?php _e("Read More") ?></a>
                    </div>
                </article>
                </div>
                <?php }

                 wp_reset_postdata();
             ?>
        </div>
		<?php // scrollbox

		echo $after_widget;

	}

	function update ( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['categories'] = (array) $new_instance['categories'];
        
		return $instance;
	}

	function form ( $instance ) {
		$defaults =  array('categories'=>array());

		$instance = wp_parse_args( (array) $instance, $defaults);

        // Page Links Options:


        //form_control('as_article_boxs', __('Display as Article Boxes?'), $instance, 'checkbox');

        echo '<h3>', __('Limit Posts to categories'), ':</h3>';

        $categories = get_categories();
            foreach ($categories as $category) {
                echo '<input type="checkbox" id="' . $this->get_field_id('categories') .'[]"
                      name="' . $this->get_field_name( 'categories' ) . '[]" ' .
                      checked(in_array($category->term_id,$instance['categories']), true, false) .
                      ' value="' . $category->term_id . '" /><label>'. $category->name .'</label><br/>'; 
            }
            
            if (!$categories) {
                echo "No categories are defined.";
            }


        echo '<hr/>';

	}

	function form_control($name, $label, $instance, $type) {
		$id = $this->get_field_id($name);
		$value = ($type=='checkbox')? 
		             checked(true, isset($instance[$name])?$instance[$name]:false, false):
		             'value="'. $instance[$name] .'"';
		printf('<p><label for="%s">%s </label> <input id="%s" name="%s" %s type="%s"/></p>',
		       $id, __($label,'latest_posts_widget'), $id,
		       $this->get_field_name($name), $value, $type);
	}

    function form_select($name, $label, $instance, $options) {
        echo '<label>' . __($label) . ' <select name="'. $this->get_field_name($name) . '">';
            foreach ($options as $id => $title) {
                echo '<option value="'. $id . '" '
                     . ( $instance[$name] == $id ? "selected" : "" )
                     .'>' . $title . '</option>';
            }
            echo '</select></label><br/>';

    }


}

?>
