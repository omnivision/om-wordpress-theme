<?php
/**
 * OM 2014 functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage OM2014
 * @since OM 2014 1.0
 */

/**
 * Sets up the content width value based on the theme's design.
 * @see om2014_content_width() for template-specific adjustments.
 */
if ( ! isset( $content_width ) )
	$content_width = 604;


/**
 * Adds support for a custom header image.
 */
require get_template_directory() . '/inc/custom-header.php';
require_once get_template_directory() . '/omscroller.php';
require_once get_template_directory() . '/latestposts_widget.php';

/**
 * OM 2014 only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
	require get_template_directory() . '/inc/back-compat.php';

/**
 * Sets up theme defaults and registers the various WordPress features that
 * OM 2014 supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since OM 2014 1.0
 *
 * @return void
 */
function om2014_setup() {
	/*
	 * Makes OM 2014 available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on OM 2014, use a find and
	 * replace to change 'om2014' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'om2014', get_template_directory() . '/languages' );
	load_theme_textdomain( 'omscroller', get_template_directory() . '/languages' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
    // add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Navigation Menu', 'om2014' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 400, 300, true );
    add_image_size('square', 200, 200, false);

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

}
add_action( 'after_setup_theme', 'om2014_setup' );


function om2014_inject_scripts() {
	wp_enqueue_script( 'functions.js', get_template_directory_uri() . '/js/functions.js', false, false, true);
}
add_action('wp_enqueue_scripts', 'om2014_inject_scripts');

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since OM 2014 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function om2014_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'om2014' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'om2014_wp_title', 10, 2 );

/**
 * Registers two widget areas.
 *
 * @since OM 2014 1.0
 *
 * @return void
 */
function om2014_widgets_init() {

    // Top bar on Front page only.
	register_sidebar( array(
		'name'          => __( 'Front Page Header Area', 'om2014' ),
		'id'            => 'front_page_header_area',
		'description'   => __( 'Appears on the front page (only) below the menu, in the header area', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s" style="clear:both;">',
		'after_widget'  => '</div>',
		'before_title'  => '<!--',
		'after_title'   => ' -->',
	) );

    // Front page stuff...
	register_sidebar( array(
		'name'          => __( 'Front Page Article Area', 'om2014' ),
		'id'            => 'front_page_article_area',
		'description'   => __( 'Appears on the front page (only) below the header area', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s" style="clear:both;">',
		'after_widget'  => '</div>',
		'before_title'  => '<!--',
		'after_title'   => ' -->',
	) );

    // Front page stuff...
	register_sidebar( array(
		'name'          => __( 'Front Page Lower Left Area', 'om2014' ),
		'id'            => 'front_page_lower_left_area',
		'description'   => __( 'Appears on the front page (only) below the article area', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s" style="clear:both;">',
		'after_widget'  => '</div>',
		'before_title'  => '<!--',
		'after_title'   => ' -->',
	) );
    // Front page stuff...
	register_sidebar( array(
		'name'          => __( 'Front Page Lower Right Area', 'om2014' ),
		'id'            => 'front_page_lower_right_area',
		'description'   => __( 'Appears on the front page (only) below the article area', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s" style="clear:both;">',
		'after_widget'  => '</div>',
		'before_title'  => '<!--',
		'after_title'   => ' -->',
	) );

    // Top bar on Front page only.
	register_sidebar( array(
		'name'          => __( 'Top Right Header Area', 'om2014' ),
		'id'            => 'top_right',
		'description'   => __( 'Appears to the right of the main menu section', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s" style="clear:both;">',
		'after_widget'  => '</div>',
		'before_title'  => '<!--',
		'after_title'   => ' -->',
	) );



    // Actual Side-bar, on, you know, the side.:

	register_sidebar( array(
		'name'          => __( 'Article Side Widget Area', 'om2014' ),
		'id'            => 'sidebar_1',
		'description'   => __( 'Appears on the right hand side on articles', 'om2014' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

    // Footer stuff.

	register_sidebar( array(
		'name'          => __( 'Footer Top', 'om2014' ),
		'id'            => 'footer_top',
		'description'   => __( 'Appears in the footer area, at the top', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Lower-Left', 'om2014' ),
		'id'            => 'footer_left',
		'description'   => __( 'Appears in the footer area, on the left', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Lower-Center', 'om2014' ),
		'id'            => 'footer_center',
		'description'   => __( 'Appears in the footer area, on the left', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Lower-Right', 'om2014' ),
		'id'            => 'footer_right',
		'description'   => __( 'Appears in the footer area, on the left', 'om2014' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'om2014_widgets_init' );

if ( ! function_exists( 'om2014_paging_nav' ) ) :
/**
 * Displays navigation to next/previous set of posts when applicable.
 *
 * @since OM 2014 1.0
 *
 * @return void
 */
function om2014_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'om2014' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'om2014' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'om2014' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'om2014_post_nav' ) ) :
/**
 * Displays navigation to next/previous post when applicable.
*
* @since OM 2014 1.0
*
* @return void
*/
function om2014_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'om2014' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'om2014' ) ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'om2014' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'om2014_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own om2014_entry_meta() to override in a child theme.
 *
 * @since OM 2014 1.0
 *
 * @return void
 */
function om2014_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'om2014' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		om2014_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'om2014' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'om2014' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}
	// Post author
	/* Don't want to disclose (admin name)
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'om2014' ), get_the_author() ) ),
			get_the_author()
		);
	}
	*/
}
endif;

if ( ! function_exists( 'om2014_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own om2014_entry_date() to override in a child theme.
 *
 * @since OM 2014 1.0
 *
 * @param boolean $echo Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function om2014_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'om2014' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'om2014' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

/**
 * Returns the URL from the post.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since OM 2014 1.0
 *
 * @return string The Link format URL.
 */
function om2014_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since OM 2014 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function om2014_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'om2014_body_class' );

/**
 * Adjusts content_width value for video post formats and attachment templates.
 *
 * @since OM 2014 1.0
 *
 * @return void
 */
function om2014_content_width() {
	global $content_width;

	if ( is_attachment() )
		$content_width = 724;
	elseif ( has_post_format( 'audio' ) )
		$content_width = 484;
}
add_action( 'template_redirect', 'om2014_content_width' );

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since OM 2014 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function om2014_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	#$wp_customize->add_section( 'dans_settings', array('title'=> 'Dans Settings', 'priority'=>201));
	$wp_customize->add_setting( 'site_logo', array('default'=> get_template_directory_uri() . '/img/om_logo.png', 'transport'=> 'postMessage'));
	$wp_customize->add_setting( 'show_site_logo', array('default'=>true, 'transport'=> 'postMessage'));

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'show_site_logo',
			array(
				'label' => 'Show Logo Image?',
				'settings' => 'show_site_logo',
				'section' => 'title_tagline',
				'type' => 'checkbox'
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'site_logo',
			array(
				'label' => 'OM Site Logo',
				'settings' => 'site_logo',
				'section' => 'title_tagline'
			)
		)
	);

	$wp_customize->add_section( 'comments', array('title'=>'Comments', 'priority'=>2));
	$wp_customize->add_setting( 'comments_on_front_page', array('default'=>false, 'transport'=>'postMessage'));
	$wp_customize->add_setting( 'comments_on_other_pages', array('default'=>false, 'transport'=>'postMessage'));

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'comments_on_front_page',
			array(
				'label' => 'Comments on Front Page?',
				'settings' => 'comments_on_front_page',
				'section' => 'comments',
				'type' => 'checkbox'
			)
		)
	);

	
	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'comments_on_other_pages',
			array(
				'label' => 'Comments on other pages?',
				'settings' => 'comments_on_other_pages',
				'section' => 'comments',
				'type' => 'checkbox'
			)
		)
	);

}
add_action( 'customize_register', 'om2014_customize_register' );

/**
 * Binds JavaScript handlers to make Customizer preview reload changes
 * asynchronously.
 *
 * @since OM 2014 1.0
 */
function om2014_customize_preview_js() {
	//wp_enqueue_script( 'om2014-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '1', true );
	wp_enqueue_script( 'om2014-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), time(), true );
}
add_action( 'customize_preview_init', 'om2014_customize_preview_js' );

/**
 * Qtranslate only filters posts by language when it's not on a 'singular' page.  Since we usually
 * want to use qtranslate on plugins which may be displayed as part of singular pages, we need
 * to duplicate their filter, but without the 'is_singular' check.
 * To use this:
 *
 * add_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);
 * $query = new WP_Query ( ... )
 * remove_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);
 *
 */
function om_qtranslate_lang_filter($where, &$wp_query)
{
        global $q_config, $wpdb;
        if(isset($q_config) && ($q_config['hide_untranslated'])) {
                $where .= " AND $wpdb->posts.post_content LIKE '%<!--:".qtrans_getLanguage()."-->%'";
                // still can't belive wordpress allows/encourages writing queries like this...
        }
        return $where;
}
