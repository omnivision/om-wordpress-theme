��          |      �          2   !     T     d  
   �     �     �     �  	   �     �     �     �  �  �  9   �     �  )   �                              #     )  #   F                   
      	                                  Caring for the sick, comforting the broken-hearted Church Planting Empowering the next generation Evangelism Give Go Justice Mentoring Pray Relief and Development The DNA of God's global church. Project-Id-Version: OM 2014 1.0
Report-Msgid-Bugs-To: http://wordpress.org/support/theme/om-theme
POT-Creation-Date: 2014-04-24 14:01:50+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2014-04-24 15:41+0000
Last-Translator: 
Language-Team: American English <kde-i18n-doc@kde.org>
X-Generator: Lokalize 1.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Prendre soin des malades, réconforter les cœurs brisés L'implantation d'églises Responsabiliser la prochaine génération EV Donner Aller Justice Mentorat Prier Urgence et de développement L'ADN de l'église mondiale de Dieu 