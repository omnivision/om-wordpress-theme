# Copyright (C) 2014 the WordPress team & OMNIvision
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: OM 2014 1.0\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/theme/om-theme\n"
"POT-Creation-Date: 2014-04-24 14:01:50+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: 404.php:16
msgid "Not found"
msgstr ""

#: 404.php:21
msgid "This is somewhat embarrassing, isn&rsquo;t it?"
msgstr ""

#: 404.php:22
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr ""

#: archive.php:29
msgid "Daily Archives: %s"
msgstr ""

#: archive.php:31
msgid "Monthly Archives: %s"
msgstr ""

#: archive.php:31
msgctxt "monthly archives date format"
msgid "F Y"
msgstr ""

#: archive.php:33
msgid "Yearly Archives: %s"
msgstr ""

#: archive.php:33
msgctxt "yearly archives date format"
msgid "Y"
msgstr ""

#: archive.php:35
msgid "Archives"
msgstr ""

#: author-bio.php:16
msgid "About %s"
msgstr ""

#: author-bio.php:20
msgid "View all posts by %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: author.php:30
msgid "All posts by %s"
msgstr ""

#: category.php:19
msgid "Category Archives: %s"
msgstr ""

#: comments.php:34
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:54
msgid "Comment navigation"
msgstr ""

#: comments.php:55
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:56
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:61
msgid "Comments are closed."
msgstr ""

#: content-aside.php:13 content-audio.php:24 content-gallery.php:24
#: content-image.php:23 content-link.php:24 content-quote.php:13
#: content-status.php:13 content-video.php:23
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: content-aside.php:14 content-audio.php:25 content-chat.php:24
#: content-gallery.php:25 content-image.php:24 content-link.php:25
#: content-quote.php:14 content-status.php:14 content-video.php:24
#: content.php:65 image.php:47 page.php:36
msgid "Pages:"
msgstr ""

#: content-aside.php:20 content-aside.php:28 content-audio.php:31
#: content-chat.php:29 content-gallery.php:39 content-image.php:35
#: content-link.php:19 content-quote.php:25 content-status.php:19
#: content-video.php:35 content.php:132 image.php:76 page.php:40
msgid "Edit"
msgstr ""

#: content-gallery.php:36 content-image.php:32 content-quote.php:22
#: content-video.php:32 content.php:123
msgid "Leave a comment"
msgstr ""

#: content-gallery.php:36 content-image.php:32 content-quote.php:22
#: content-video.php:32 content.php:123
msgid "One comment so far"
msgstr ""

#: content-gallery.php:36 content-image.php:32 content-quote.php:22
#: content-video.php:32 content.php:123
msgid "View all % comments"
msgstr ""

#: content-none.php:12
msgid "Nothing Found"
msgstr ""

#: content-none.php:18
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:22
msgid ""
"Sorry, but nothing matched your search terms. Please try again with "
"different keywords."
msgstr ""

#: content-none.php:27
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: content.php:74
msgid "Credit"
msgstr ""

#: content.php:111
msgid "Contact:"
msgstr ""

#: content.php:140 latestposts_widget.php:71 omscroller.php:159
msgid "Read More"
msgstr ""

#. #-#-#-#-#  om-theme.pot (OM 2014 1.0)  #-#-#-#-#
#. Author URI of the plugin/theme
#: footer.php:45
msgid "http://wordpress.org/"
msgstr ""

#: footer.php:46
msgid "Semantic Personal Publishing Platform"
msgstr ""

#: footer.php:47
msgid "Proudly powered by %s"
msgstr ""

#: functions.php:96
msgid "Navigation Menu"
msgstr ""

#: functions.php:139
msgid "Page %s"
msgstr ""

#: functions.php:156
msgid "Front Page Header Area"
msgstr ""

#: functions.php:158
msgid "Appears on the front page (only) below the menu, in the header area"
msgstr ""

#: functions.php:167
msgid "Front Page Article Area"
msgstr ""

#: functions.php:169
msgid "Appears on the front page (only) below the header area"
msgstr ""

#: functions.php:178
msgid "Front Page Lower Left Area"
msgstr ""

#: functions.php:180 functions.php:190
msgid "Appears on the front page (only) below the article area"
msgstr ""

#: functions.php:188
msgid "Front Page Lower Right Area"
msgstr ""

#: functions.php:199
msgid "Top Right Header Area"
msgstr ""

#: functions.php:201
msgid "Appears to the right of the main menu section"
msgstr ""

#: functions.php:213
msgid "Article Side Widget Area"
msgstr ""

#: functions.php:215
msgid "Appears on the right hand side on articles"
msgstr ""

#: functions.php:225
msgid "Footer Top"
msgstr ""

#: functions.php:227
msgid "Appears in the footer area, at the top"
msgstr ""

#: functions.php:235
msgid "Footer Lower-Left"
msgstr ""

#: functions.php:237 functions.php:247 functions.php:256
msgid "Appears in the footer area, on the left"
msgstr ""

#: functions.php:245
msgid "Footer Lower-Center"
msgstr ""

#: functions.php:254
msgid "Footer Lower-Right"
msgstr ""

#: functions.php:281
msgid "Posts navigation"
msgstr ""

#: functions.php:285
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr ""

#: functions.php:289
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: functions.php:317
msgid "Post navigation"
msgstr ""

#: functions.php:320
msgctxt "Previous post link"
msgid "<span class=\"meta-nav\">&larr;</span> %title"
msgstr ""

#: functions.php:321
msgctxt "Next post link"
msgid "%title <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: functions.php:341
msgid "Sticky"
msgstr ""

#. Translators: used between list items, there is a space after the comma.
#: functions.php:347 functions.php:353
msgid ", "
msgstr ""

#: functions.php:361
msgid "View all posts by %s"
msgstr ""

#: functions.php:381
msgctxt "1: post format name. 2: date"
msgid "%1$s on %2$s"
msgstr ""

#: functions.php:387
msgid "Permalink to %s"
msgstr ""

#: header.php:98
msgid "Menu"
msgstr ""

#: header.php:99
msgid "Skip to content"
msgstr ""

#: image.php:23
msgid "<span class=\"meta-nav\">&larr;</span> Previous"
msgstr ""

#: image.php:24
msgid "Next <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: image.php:54
msgid ""
"<span class=\"attachment-meta\">Published on <time class=\"entry-date\" "
"datetime=\"%1$s\">%2$s</time> in <a href=\"%3$s\" title=\"Return to %4$s\" "
"rel=\"gallery\">%5$s</a></span>"
msgstr ""

#: image.php:70
msgid "Link to full-size image"
msgstr ""

#: image.php:71
msgid "Full resolution"
msgstr ""

#: inc/back-compat.php:38 inc/back-compat.php:50 inc/back-compat.php:65
msgid ""
"OM 2014 requires at least WordPress version 3.6. You are running version %s. "
"Please upgrade and try again."
msgstr ""

#: latestposts_widget.php:36
msgid "A simple widget for showing the latest posts as items, not just a list."
msgstr ""

#: latestposts_widget.php:41
msgid "Latest Posts Widget"
msgstr ""

#: latestposts_widget.php:103
msgid "Limit Posts to categories"
msgstr ""

#: omscroller.php:47
msgid ""
"A Scrolling Widget that shows the OM Focus Areas, and Pray Give Go logos."
msgstr ""

#: omscroller.php:64
msgid "OM Scroller"
msgstr ""

#: omscroller.php:107
msgid "Church Planting"
msgstr ""

#: omscroller.php:108
msgid "The DNA of God's global church."
msgstr ""

#: omscroller.php:111
msgid "Evangelism"
msgstr ""

#: omscroller.php:112
msgid "\"Go and make disciples of all Nations\" - Jesus"
msgstr ""

#: omscroller.php:115
msgid "Justice"
msgstr ""

#: omscroller.php:116
msgid "God is serious about Justice. So are we."
msgstr ""

#: omscroller.php:119
msgid "Relief and Development"
msgstr ""

#: omscroller.php:120
msgid "Caring for the sick, comforting the broken-hearted"
msgstr ""

#: omscroller.php:123
msgid "Mentoring"
msgstr ""

#: omscroller.php:124
msgid "Empowering the next generation"
msgstr ""

#: omscroller.php:134
msgid "Pray"
msgstr ""

#: omscroller.php:135
msgid "OM is committed to prayer.  How else could be be here at all?"
msgstr ""

#: omscroller.php:137
msgid "Give"
msgstr ""

#: omscroller.php:138
msgid "None of this would be possible without the generosity of the saints."
msgstr ""

#: omscroller.php:140
msgid "Go"
msgstr ""

#: omscroller.php:141
msgid ""
"Every 24 hours over 350000 people are born.  15000 die.  All of them need "
"the Saviour."
msgstr ""

#: search.php:18
msgid "Search Results for: %s"
msgstr ""

#: tag.php:21
msgid "Tag Archives: %s"
msgstr ""

#: taxonomy-post_format.php:22
msgid "%s Archives"
msgstr ""

#. Theme Name of the plugin/theme
msgid "OM 2014"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://wordpress.org/themes/om2014"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Taking the Wordpress \"Twenty Thirteen\" theme, and modifying the heck out "
"of it, until it looks something like the roksprocket theme that Chris made/"
"uses."
msgstr ""

#. Author of the plugin/theme
msgid "the WordPress team & OMNIvision"
msgstr ""
