<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage OM2014
 * @since OM 2014 1.0
 */

$meta = array();
foreach (get_post_custom_keys() as $k) {
    $v = get_post_meta(get_the_ID(), $k);
    $meta[$k] = $v[0]?$v[0]:'';
}

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if ( is_single() ) : ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <span class="top_metadata">
            <?php
                $date = $meta['_creation_date'];
                if ($date) {
                    echo '<span class="creation_date">' . $date . '</span>';
                };
                $country = $meta['Country'];
                if ($country) {
                    echo '<span class="country">' . $country . '</span>';
                }
            ?>
            </span>
            <?php if ( has_post_thumbnail() ) : ?>

                <div class="entry-thumbnail">
                <?php $large = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
                    <a href="<?php echo $large[0]; ?>"><?php the_post_thumbnail('medium'); ?></a>
                </div>

            <?php endif; ?>

		<?php else : ?>
            <h1 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h1>

            <?php if ( has_post_thumbnail() ) : ?>

                <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail(); ?></a>
                </div>

            <?php endif; ?>

		<?php endif; // is_single() ?>

	</header><!-- .entry-header -->

    <?php if (is_single()): ?>

        <div class="entry-content">
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">'
                                                    . __( 'Pages:', 'om2014' ) . '</span>',
                                        'after' => '</div>',
                                        'link_before' => '<span>',
                                        'link_after' => '</span>' ) ); ?>
        </div><!-- .entry-content -->

        <span class="copyright">
        <?php

        $credit_prefix = __('Credit'). ': ';

        $credit = $meta['_credit'];
        $copyright = $meta['_copyright'];
        $subject = "OM Article ".$meta['_caleb_id']." ".get_the_title();
        $om_email = '<a class="email" href="mailto:editor@om.org?subject='. $subject . '">OM International</a>';

        switch($credit) {
            case "No need for credit - Credit as OM":
                echo $credit_prefix."OM International";
                break;
            case "Author/Creator must be credited - wording of credit detailed by owner":
                echo  $credit_prefix.'<a class="email" href="mailto://' . $meta['_author_email'] . '?subject='. $subject . '">',
                        $meta['_author_name'] . '</a>';
                break;
            case "Author/Creator must be credited and email must be sent to owner":
                echo  $credit_prefix.'<a class="email" href="mailto://' . $meta['_author_email'] . '?subject='. $subject . '">',
                        $meta['_author_name'] . '</a>';
                break;
            case "Contact owner for copyright details":
                echo  $credit_prefix.'<a class="email" href="mailto://' . $meta['_author_email'] . '?subject='. $subject . '">',
                        $meta['_author_name'] . '</a>';
                break;
            default:
                break;
            }
        echo " "; 
        switch($copyright) {
        case 'No copyright (Copyright ceded to OM)':
            echo '&copy;', substr($meta['_creation_date'],-4), ' ', $om_email;
            break;
        case 'Owner has copyright':
            echo '&copy;', substr($meta['_creation_date'],-4).' ';
            echo  '<a class="email" href="mailto://' . $meta['_owner_email'] . '?subject='. $subject . '">',
                    $meta['_owner_name'] . '</a>';
            break;
        case "Contact Owner for Copyright Details":
            echo '&copy;', substr($meta['_creation_date'],-4), '. ',__('Contact:', 'OM-2014');
            echo  '<a class="email" href="mailto://' . $meta['_owner_email'] . '?subject='. $subject . '">',
                    $meta['_owner_name'] . '</a>';
            break;
        default: break;

        }
    ?>

	<footer class="entry-meta">
		<?php if ( comments_open() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'om2014' ) . '</span>', __( 'One comment so far', 'om2014' ), __( 'View all % comments', 'om2014' ) ); ?>
			</div><!-- .comments-link -->
		<?php endif; // comments_open() ?>

		<?php if ( get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>

        <?php om2014_entry_meta(); ?>
        <?php edit_post_link( __( 'Edit', 'om2014' ), '<span class="edit-link">', '</span>' ); ?>
        <?php the_meta(); ?>

	</footer><!-- .entry-meta -->

	<?php else: // not single. ?>

        <div class="entry-summary">
            <?php the_content( __( 'Read More', 'om2014' ) ); ?>
        </div><!-- .entry-summary -->

	<?php endif; ?>

    <div class="cf"></div>
</article><!-- #post -->
