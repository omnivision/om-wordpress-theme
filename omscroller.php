<?php
/**
 * Plugin Name: OM Scroller
 * Description: A Scrolling Widget that shows the OM focus areas, and pray-give-go logos.
 * Version: 0.1
 * Author: Daniel Fairhead
 * Author URI: http://www.om.org/
 */

add_action( 'widgets_init', 'register_om_scroller' );

function register_om_scroller()
{
    register_widget( 'OM_Scroller' );
}

class Collapsable
{
    /* In a widget, if you want a section to be collapsable, use this.
        Collapsable::start('Links');
           echo 'stuff inside the widget...';
        Collapsable::end();
    */
    public function start($title, $id)
    {
        echo '<div class="widget ' . $id . '"><div class="widget-top" style="cursor:pointer"><div class="widget-title-action"><a class="widget-action hide-if-no-js" href="#"></a></div><div class="widget-title"><h5>', __($title, 'omscroller'), '</h5></div></div><div class="widget-inside">';
    }
    public function end()
    {
        echo '</div></div>';
    }

    public function hide_by($checkbox, $id)
    {
        // hide a section ($id) depending on value of $checkbox (a name given by "$this->get_field_name")

        ?><script>
            (function($){
                var s=$('input[name="<?php echo $checkbox;?>"]'),
                    x=function(){ s.parent().siblings('.<?php echo $id ?>').toggle(s.prop("checked"));};
                x();
                s.click(x);
            })(jQuery)</script><?php
    }
}


class OM_Scroller extends WP_Widget
{

    function OM_Scroller()
    {
        $widget_ops = array( 'classname' => 'omscroller',
                           'description' => __('A Scrolling Widget that shows the OM Focus Areas, and Pray Give Go logos.', 'omscroller'));
        $control_ops = array('height' => 300,
                             'show_areas'=>1,
                             'show_pgg'=>1,
                             'show_posts'=>0,
                             'show_time'=>9000,
                             'id_base' => 'omscroller',
                             'link_pray'=>0,
                             'link_give'=>0,
                             'link_go'=>0,
                             'link_church_planting'=>0,
                             'link_evangelism'=>0,
                             'link_justice'=>0,
                             'link_relief_and_development'=>0,
                             'link_mentoring'=>0,
                             'pgg_order'=>'link_pray,link_give,link_go',
                             'area_order'=>'link_church_planting,link_evangelism,link_justice,link_relief_and_development,link_mentoring',
                             'categories'=>array());

        $this->WP_Widget( 'omscroller', __('OM Scroller', 'omscroller'), $widget_ops, $control_ops);

    }

    private static function omarea($name, $title, $desc, $url, $img_dir)
    {
    ?>
        <a class=" col span_1_of_5" href="<?php echo get_permalink($url); ?>">
            <div class="omarea <?php echo $name; ?>">
                <img src="<?php echo $img_dir . $name . '.png'; ?>" alt="<?php echo $title; ?>" />
                <div class="title"><?php echo $title; ?></div>
                <div class="desc"><h3><?php echo $title; ?></h3><p><?php echo $desc; ?></p></div>
            </div>
        </a>
    <?php
    }

    private static function pgg($name, $title, $desc, $url, $img_dir)
    {
    ?>
        <a class=" col span_1_of_5" href="<?php echo get_permalink($url); ?>">
            <div class="omarea <?php echo $name; ?>">
                <img src="<?php echo $img_dir . $name . '.png'; ?>" alt="<?php echo $title; ?>" />
                <h2 class="title"><?php echo strtolower($title); ?></h2>
                <div class="desc"><h3><?php echo $title; ?></h3><p><?php echo $desc; ?></p></div>
            </div>
        </a>
    <?php
    }

    function wpml_localize( &$instance) {
        if (!function_exists('wpml_get_content')) {
            return 0;
        }
        $om_things = array("pray","give","go",
                           "church_planting","evangelism","justice",
                           "relief_and_development","mentoring");

        foreach ($om_things as $t) {
            if (isset($instance["link_$t"])) {
                $wpml_link = wpml_get_content('post_page', (int)$instance["link_$t"] );
                if ($wpml_link) {
                    $instance["link_$t"] = $wpml_link;
                }
            }
        }

    }

    function widget( $args, $instance ) {
        extract ($args);
        $show_info = isset( $instance['show_info'] ) ? $instance['show_info'] : false;

        if ($instance['show_areas'] || $instance['show_pgg']) {
		$this->wpml_localize($instance); 
        }

        echo $before_widget;

        $img_dir = get_template_directory_uri() . '/img/';

        ?>
            <div class="omscroller-box" style="overflow: hidden">
            <?php if($instance['show_areas']) { ?>
            <div class="omareas group">
                <?php 
                    foreach (split(',', $instance['area_order']) as $area) {
                        switch ($area) {
                        case 'link_church_planting':
                            OM_Scroller::omarea('church_planting', __('Church Planting', 'om2014'),
                                   __('The DNA of God\'s global church.', 'om2014'),
                                   $instance['link_church_planting'], $img_dir);
                            break;
                        case 'link_evangelism':
                            OM_Scroller::omarea('evangelism', __('Evangelism', 'om2014'),
                                   __('"Go and make disciples of all Nations" - Jesus', 'om2014'),
                                   $instance['link_evangelism'], $img_dir);
                            break;
                        case 'link_justice':
                            OM_Scroller::omarea('justice', __('Justice', 'om2014'),
                                   __('God is serious about Justice. So are we.', 'om2014'),
                                   $instance['link_justice'], $img_dir);
                            break;
                        case 'link_relief_and_development':
                            OM_Scroller::omarea('relief_and_development', __('Relief and Development', 'om2014'),
                                   __('Caring for the sick, comforting the broken-hearted', 'om2014'),
                                   $instance['link_relief_and_development'], $img_dir);
                            break;
                        case 'link_mentoring':
                            OM_Scroller::omarea('mentoring', __('Mentoring', 'om2014'),
                                   __('Empowering the next generation', 'om2014'),
                                   $instance['link_mentoring'], $img_dir);
                            break;
                        }
                    }
	   ?>
            </div>
            <?php } // omareas

            if($instance['show_pgg']) { ?>
            <div id="pgg-wrapper" class="pgg-wrapper omareas group">
		<div class="col span_1_of_5">&nbsp;</div>
                <?php
                    foreach (split(',', $instance['pgg_order']) as $pgg) {
                        switch ($pgg) {
                        case 'link_pray':
                            OM_Scroller::pgg('pgg-pray', __('Pray', 'om2014'),
                                   __('OM is committed to prayer.  How else could we be here at all?', 'om2014'),
                                   $instance['link_pray'], $img_dir);
                            break;
                        case 'link_give':
                            OM_Scroller::pgg('pgg-give', __('Give', 'om2014'),
                                   __('None of this would be possible without the generosity of the saints.', 'om2014'),
                                   $instance['link_give'], $img_dir);
                            break;
                        case 'link_go':
                            OM_Scroller::pgg('pgg-go', __('Go', 'om2014'),
                                   __('Every 24 hours over 350000 people are born.  15000 die.  All of them need the Saviour.', 'om2014'),
                               $instance['link_go'], $img_dir);
                            break;
                        }
                    }

                ?>
		<div class="col span_1_of_5">&nbsp;</div>
            </div>
            <?php } // pgg
            if ($instance['show_posts']) { 

                add_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);
                $query = new WP_Query(array('category__in'=>$instance['categories']));
                remove_filter('posts_where', 'om_qtranslate_lang_filter', 10, 2);

                while ($query->have_posts()) {
                $query->the_post();

                ?><div class="group omscroller_post">
                    <div class="col span_3_of_5">
                        <a href="<?php echo get_permalink(); ?>"><h3><?php the_title(); ?></h3><?php the_excerpt();?></a>
                    <a class="more-link" href="<?php echo get_permalink(); ?>"><?php _e("Read More") ?></a>
                    </div>
                    <div class="col span_2_of_5" style="text-align:center"><?php the_post_thumbnail(); ?></div>
                </div>
                <?php }

                 wp_reset_postdata();
             } // show_posts
             ?>
            <script>
		   // inline javascript to stop flash of to-be-hidden-content.  not pure css for noscript support
	           var box=document.scripts[document.scripts.length -1].parentNode.children;
                   for(var i=box.length-1;i>0;i--){
                       box[i].style.display="none";
                   }
            </script>
            </div>
        <?php // scrollbox

        echo $after_widget;
        ?><script>_defer(function($){$("#<?php echo $this->id; ?> > .omscroller-box").scrollbox(<?php echo $instance['show_time']; ?>);});</script><?php

    }

    function update ( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['height'] = (int) $new_instance['height'];
        $instance['show_areas'] = $new_instance['show_areas']=='on';
        $instance['show_pgg'] = $new_instance['show_pgg']=='on';
        $instance['show_posts'] = $new_instance['show_posts']=='on';
        $instance['show_time'] = (int) $new_instance['show_time']?:$old_instance['show_time'];
        $instance['categories'] = (array) $new_instance['categories'];

        ///////////////////////////////////////////////////////////////////////

        $valid_pggs = array('link_pray', 'link_give', 'link_go');
        $instance['pgg_order'] = array();
        $i = FALSE;

        foreach (split(',', $new_instance['pgg_order']) as $pgg) {
            $i = array_search($pgg, $valid_pggs);
            if ($i !== FALSE) {
                $instance['pgg_order'][] = $pgg;
                unset($valid_pggs[$i]);
            }
        }

        $instance['pgg_order'] = join(',', array_merge($instance['pgg_order'], $valid_pggs));

        ///////////////////////////////////////////////////////////////////////

        $valid_areas = array('link_church_planting', 'link_evangelism',
                             'link_justice', 'link_relief_and_development',
                             'link_mentoring');

        $instance['area_order'] = array();
        $i = FALSE;

        foreach (split(',', $new_instance['area_order']) as $area) {
            $i = array_search($area, $valid_areas);
            if ($i !== FALSE) {
                $instance['area_order'][] = $area;
                unset($valid_areas[$i]);
            }
        }

        $instance['area_order'] = join(',', array_merge($instance['area_order'], $valid_areas));

        ///////////////////////////////////////////////////////////////////////

        $om_things = array("pray","give","go","church_planting","evangelism","justice","relief_and_development","mentoring");

        foreach($om_things as $t) {
            $instance['link_' . $t] = (int) $new_instance['link_' . $t];
        }

        return $instance;
    }

    function form ( $instance ) {
        $defaults =  array('height' => 300,
                             'show_areas'=>1,
                             'show_pgg'=>1,
                             'show_posts'=>1,
                             'show_time'=>9000,
                             'categories'=>array(),
                             'link_pray'=>0,
                             'pgg_order'=>'link_pray,link_give,link_go',
                             'area_order'=>'link_church_planting,link_evangelism,link_justice,link_relief_and_development,link_mentoring',
                );

        $instance = wp_parse_args( (array) $instance, $defaults);

        // General Options:

        $this->form_control('height', 'Initial Height', $instance, 'number');
        $this->form_control('show_areas', 'Show Areas?', $instance, 'checkbox');
        $this->form_control('show_pgg', 'Show Pray-Give-Go?', $instance, 'checkbox');
        $this->form_control('show_posts', 'Show Posts?', $instance, 'checkbox');

        $this->form_control('show_time', 'Time to show each page (ms)', $instance, 'number');

        // Page Links Options:

        $pages = array();

        foreach (get_pages() as $p) {
            $pages[$p->ID] = $p->post_title;
        }

        Collapsable::start('Pray/Give/Go Links', 'pgg_links');

            $link_labels = array('link_pray' => 'Pray', 'link_give' => 'Give', 'link_go' => 'Go');

            ?><div class="om_sortable_list"><?php
            foreach(split(',', $instance['pgg_order']) as $pgg) {
                $this->form_select($pgg, $link_labels[$pgg] . ':', $instance, $pages);
            }
            ?></div>
            <input type="hidden" name="<?php echo $this->get_field_name('pgg_order'); ?>"
                   class="order_list" value="<?php echo $instance['pgg_order']; ?>" />
            <?php
            

        Collapsable::end();

        Collapsable::hide_by($this->get_field_name('show_pgg'), 'pgg_links');

        Collapsable::start('Area Links', 'area_links');

            $link_labels = array('link_church_planting' => 'Church Planting',
                                 'link_evangelism' => 'Evangelism',
                                 'link_justice' => 'Justice',
                                 'link_relief_and_development' => 'Relief and Development',
                                 'link_mentoring' => 'Mentoring');

            ?><div class="om_sortable_list"><?php
            foreach(split(',', $instance['area_order']) as $area) {
                $this->form_select($area, $link_labels[$area] . ':', $instance, $pages);
            }
            ?></div>
            <input type="hidden" name="<?php echo $this->get_field_name('area_order'); ?>"
                   class="order_list" value="<?php echo $instance['area_order']; ?>" />
            <?php

        Collapsable::end();

        Collapsable::hide_by($this->get_field_name('show_areas'), 'area_links');

        // Tagged Posts options:

        Collapsable::start('Limit posts to categories...', 'post_categories');

            foreach (get_categories() as $category) {
                echo '<input type="checkbox" id="' . $this->get_field_id('categories') .'[]"
                      name="' . $this->get_field_name( 'categories' ) . '[]" ' .
                      checked(in_array($category->term_id,$instance['categories']), true, false) .
                      ' value="' . $category->term_id . '" /><label>'. $category->name .'</label><br/>'; 
            }
            
            if (!$categories) {
                echo "No categories are defined.";
            }

        Collapsable::end();

        Collapsable::hide_by($this->get_field_name('show_posts'), 'post_categories');

        echo '<hr/>';

        ?>
            <script defer>
            jQuery(function() {
                jQuery('.om_sortable_list').sortable({
                    update: function(e, ui) {
                        ui.item.parent().siblings('input.order_list').val(
                            jQuery.map(ui.item.parent().children('.item'),
                                function(n){return jQuery(n).data('id')}).join(','));
                    }
                });
            });
            </script>
        <?php

    }

    function form_control($name, $label, $instance, $type) {
        $id = $this->get_field_id($name);
        $value = ($type=='checkbox')? 
                     checked(true, isset($instance[$name])?$instance[$name]:false, false):
                     'value="'. $instance[$name] .'"';
        printf('<p><label for="%s">%s </label> <input id="%s" name="%s" %s type="%s"/></p>',
               $id, __($label,'omscroller'), $id,
               $this->get_field_name($name), $value, $type);
    }

    function form_select($name, $label, $instance, $options) {
        echo '<div class="item" data-id="'. $name . '"><label>' . __($label) . ' <select name="'. $this->get_field_name($name) . '">';
            foreach ($options as $id => $title) {
                echo '<option value="'. $id . '" '
                     . ( $instance[$name] == $id ? "selected" : "" )
                     .'>' . $title . '</option>';
            }
            echo '</select></label></div>';

    }


}

?>
