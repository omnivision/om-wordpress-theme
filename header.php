<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage OM2014
 * @since OM 2014 1.0
 */

function css_uri($name) {
	return get_template_directory_uri() . "/css/" . $name;
}

function img_uri($name) {
	return get_template_directory_uri() . "/img/" . $name;
}

function print_if($format_str, $value){
	if ($value) {
		printf($format_str, $value);
	}
}

function print_if_else($format_str, $value, $default){
	printf($format_str, $value?:$default);
}



?><!DOCTYPE html>
<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta http-equiv="cleartype" content="on">

	<link rel="shortcut icon" href="/favicon.ico" />

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True" />
	<meta name="MobileOptimized" content="320" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Stylesheets -->
	<!--
	<link rel="stylesheet" href="<?php echo css_uri("html5reset.css") ?>" media="all" />
	<link rel="stylesheet" href=<?php echo css_uri("col.css"); ?> media="all" />
	<link rel="stylesheet" href=<?php echo css_uri("2cols.css"); ?> media="all" />
	<link rel="stylesheet" href=<?php echo css_uri("3cols.css"); ?> media="all" />
	<link rel="stylesheet" href=<?php echo css_uri("4cols.css"); ?> media="all" />
	<link rel="stylesheet" href=<?php echo css_uri("5cols.css"); ?> media="all" />
	-->
	<link href='http://fonts.googleapis.com/css?family=Josefin+Slab:600italic|Source+Sans+Pro:700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href=<?php echo get_template_directory_uri() . '/style.css'; ?> media="all" />
	<?php wp_head(); ?>

	<!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements and feature detects -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <script>// and this jQuery hack, which allows injection of jQuery dependent chunks of code.
    var _defer=(function(){var defers=[];window.onload=function(){while(defers.length){defers.pop()(window.jQuery);}};return function(x){defers.push(x);};}());
    </script>

</head>

<body <?php body_class(); ?>>
    <div class="colors_bar"></div>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header main_bg" role="banner">
        <div class="group">
			<div class="col logo">
				<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php if (get_theme_mod('show_site_logo')) {print_if_else('<img src="%s" />', get_theme_mod( 'site_logo'), img_uri('om_logo.png'));} ?>
					<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</a>
			</div>
            <div id="top_right" class="col">
                <?php dynamic_sidebar('top_right'); ?>
            </div>
			<div id="nav" class="nav col unclicked">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( 'Menu', 'om2014' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'om2014' ); ?>"><?php _e( 'Skip to content', 'om2014' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
        </div>
		<?php if(is_home()||is_front_page()) {dynamic_sidebar('front_page_header_area');} ?>

		</header><!-- #masthead -->

		<div id="main" class="site-main">

		<?php if(is_home()||is_front_page()):?>
            <?php dynamic_sidebar('front_page_article_area'); ?> 
        <?php endif ?>

