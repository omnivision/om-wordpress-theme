<?php
/**
 Template Name: Basic with Sidebar
 * The template for displaying pages with a side-bar (for widgets, etc).
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage OM2014
 * @since OM 2014 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content with-sidebar" role="main">

            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header><!-- .entry-header -->
                    <div class="entry-content">
                        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                        <div class="entry-thumbnail">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <?php endif; ?>


                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'om2014' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                    </div><!-- .entry-content -->
                    <div class="cf"></div>
                </article><!-- #post -->

                <?php comments_template(); ?>
            <?php endwhile; ?>
            <div class="sidebar">
                <?php dynamic_sidebar('sidebar_1'); ?> 
	    </div>

        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
