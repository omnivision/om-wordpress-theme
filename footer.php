<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage OM2014
 * @since OM 2014 1.0
 */
?>


		<?php if(is_home()||is_front_page()):?>
        <div class="group">
            <div class="col span_1_of_2">
            <?php dynamic_sidebar('front_page_lower_left_area'); ?> 
            </div>
            <div class="col span_1_of_2">
            <?php dynamic_sidebar('front_page_lower_right_area'); ?> 
            </div>
        </div>
        <?php endif ?>
		</div><!-- #main -->
		<footer id="colophon" class="site-footer main_bg" role="contentinfo">
            <div class="group">
               <?php dynamic_sidebar('footer_top'); ?> 
            </div>
            <div class="darker">
            <div class="group">
                <div class="col span_1_of_3">
                <?php dynamic_sidebar('footer_left'); ?> 
                </div>
                <div class="col span_1_of_3">
                <?php dynamic_sidebar('footer_center'); ?> 
                </div>
                <div class="col span_1_of_3">
                <?php dynamic_sidebar('footer_right'); ?> 
                </div>
            </div>

			<div class="site-info">
				<?php do_action( 'om2014_credits' ); ?>
			</div><!-- .site-info -->
            </div>
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.7.2.min.js"></script>
	<?php wp_footer(); ?>
<!--    <script>function sticky_footer(){var f=jQuery('footer'),h=f.height();f.css('min-height',f.height());jQuery('body').css('margin-bottom',parseInt(h,10));f.css({position:'fixed',width:'100%',bottom:0});}jQuery(sticky_footer);jQuery(window).resize(sticky_footer);</script>-->
</body>
</html>
